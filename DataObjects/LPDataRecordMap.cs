﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataObjects
{
    /// <summary>
    /// This sealed class maps LP Csv file data column to class properties
    /// </summary>
    public sealed class LPDataRecordMap : ClassMap<LPCsvDataRecord>
    {
        public LPDataRecordMap()
        {
            AutoMap();
            Map(m => m.DateTime).Name("Date/Time");
            Map(m => m.DataValue).ConvertUsing(row =>
            {
                return row.GetField<double>("Data Value");
            });
        }
    }

    /// <summary>
    /// Class to hold LP Csv data
    /// </summary>
    public class LPCsvDataRecord
    {
        public string DateTime { get; set; }
        public double DataValue { get; set; }
    }

}
