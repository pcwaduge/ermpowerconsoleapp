﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataObjects
{
    /// <summary>
    /// This sealed class maps TOU Csv file data column to class properties
    /// </summary>
    public sealed class TOUDataRecordMap : ClassMap<TOUCsvDataRecord>
    {
        public TOUDataRecordMap()
        {
            AutoMap();
            Map(m => m.DateTime).Name("Date/Time");
            Map(m => m.Energy).ConvertUsing(row =>
            {
                return row.GetField<double>("Energy");
            });
        }
    }
    /// <summary>
    /// Class to hold TOU Csv data
    /// </summary>
    public class TOUCsvDataRecord
    {
        public string DateTime { get; set; }
        public double Energy { get; set; }
    }
}
