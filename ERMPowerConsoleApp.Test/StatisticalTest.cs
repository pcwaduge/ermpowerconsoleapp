﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataObjects;
using BusinessObjects;
using System.Collections.Generic;

namespace ERMPowerConsoleApp.Test
{
    /// <summary>
    /// A test class that implements test methods for calculating median values and 20% above and below values
    /// for LP and TOU type Csv files. The test method names are self explanatory.
    /// </summary>
    [TestClass]
    public class StatisticalTest
    {
        [TestMethod]
        public void CalculateMedianForLPDataRecord()
        {
            //Arrange
            var csvDataPoint1 = new LPCsvDataRecord();
            csvDataPoint1.DataValue = 12.04;

            var csvDataPoint2 = new LPCsvDataRecord();
            csvDataPoint2.DataValue = 125.06;

            var csvDataPoint3 = new LPCsvDataRecord();
            csvDataPoint3.DataValue = 172.06;

            var csvDataPoint4 = new LPCsvDataRecord();
            csvDataPoint4.DataValue = 912.64;

            var csvDataPoint5 = new LPCsvDataRecord();
            csvDataPoint5.DataValue = 34512.74;

            var lpFile = new LPCsvFile();
            var lst = new List<LPCsvDataRecord>();
            lst.Add(csvDataPoint1);
            lst.Add(csvDataPoint2);
            lst.Add(csvDataPoint3);
            lst.Add(csvDataPoint4);
            lst.Add(csvDataPoint5);
            lpFile.ExcelData = lst;

            double expectedMedian = 172.06;

            //Act

            double calculatedMean = lpFile.FindMedian();

            //Assert

            Assert.AreEqual(expectedMedian, calculatedMean);            
        }

        [TestMethod]
        public void CalculateMedianForTOUDataRecord()
        {
            //Arrange
            var csvDataPoint1 = new TOUCsvDataRecord();
            csvDataPoint1.Energy = 712.04;

            var csvDataPoint2 = new TOUCsvDataRecord();
            csvDataPoint2.Energy = 9125.04;

            var csvDataPoint3 = new TOUCsvDataRecord();
            csvDataPoint3.Energy = 7172.04;

            var csvDataPoint4 = new TOUCsvDataRecord();
            csvDataPoint4.Energy = 9182.04;

            var csvDataPoint5 = new TOUCsvDataRecord();
            csvDataPoint5.Energy = 345812.04;

            var touFile = new TOUCsvFile();
            var lst = new List<TOUCsvDataRecord>();
            lst.Add(csvDataPoint1);
            lst.Add(csvDataPoint2);
            lst.Add(csvDataPoint3);
            lst.Add(csvDataPoint4);
            lst.Add(csvDataPoint5);
            touFile.ExcelData = lst;

            double expectedMedian = 9125.04;

            //Act

            double calculatedMean = touFile.FindMedian();

            //Assert

            Assert.AreEqual(expectedMedian, calculatedMean);
        }

        [TestMethod]
        public void Calculate20percentAboveOrBelowMedianValuesForLPDataRecord()
        {
            //Arrange
            var csvDataPoint1 = new LPCsvDataRecord();
            csvDataPoint1.DataValue = 12.04;

            var csvDataPoint2 = new LPCsvDataRecord();
            csvDataPoint2.DataValue = 155.06;

            var csvDataPoint3 = new LPCsvDataRecord();
            csvDataPoint3.DataValue = 172.06;

            var csvDataPoint4 = new LPCsvDataRecord();
            csvDataPoint4.DataValue = 180.64;

            var csvDataPoint5 = new LPCsvDataRecord();
            csvDataPoint5.DataValue = 34512.74;

            var lpFile = new LPCsvFile();
            var lst = new List<LPCsvDataRecord>();
            lst.Add(csvDataPoint1);
            lst.Add(csvDataPoint2);
            lst.Add(csvDataPoint3);
            lst.Add(csvDataPoint4);
            lst.Add(csvDataPoint5);
            lpFile.ExcelData = lst;

            double expectedMedian = 172.06;
            var expectedValuesAboveOrBelowMean = new List<double>();
            expectedValuesAboveOrBelowMean.Add(12.04);
            expectedValuesAboveOrBelowMean.Add(34512.74);

            //Act

            double calculatedMean = lpFile.FindMedian();
            var perAboveMedian = calculatedMean + calculatedMean * 0.2;
            var perBelowMedian = calculatedMean - calculatedMean * 0.2;

            var calculatedValuesAboveOrBelowMean = new List<double>();

            foreach (LPCsvDataRecord model in lpFile.ExcelData)
            {
                // values 20% above median
                if (model.DataValue > 0 && (model.DataValue > perAboveMedian))
                {
                    calculatedValuesAboveOrBelowMean.Add(model.DataValue);
                }

                // values 20% above median
                if (model.DataValue > 0 && (model.DataValue < perBelowMedian))
                {
                    calculatedValuesAboveOrBelowMean.Add(model.DataValue);
                }
            }

            //Assert

            Assert.AreEqual(expectedMedian, calculatedMean);
            CollectionAssert.AreEqual(expectedValuesAboveOrBelowMean, calculatedValuesAboveOrBelowMean);
        }

        [TestMethod]
        public void Calculate20percentAboveOrBelowMedianValuesForTOUDataRecord()
        {
            //Arrange
            var csvDataPoint1 = new TOUCsvDataRecord();
            csvDataPoint1.Energy = 712.04;

            var csvDataPoint2 = new TOUCsvDataRecord();
            csvDataPoint2.Energy = 9125.04;

            var csvDataPoint3 = new TOUCsvDataRecord();
            csvDataPoint3.Energy = 7172.04;

            var csvDataPoint4 = new TOUCsvDataRecord();
            csvDataPoint4.Energy = 9182.04;

            var csvDataPoint5 = new TOUCsvDataRecord();
            csvDataPoint5.Energy = 345812.04;

            var touFile = new TOUCsvFile();
            var lst = new List<TOUCsvDataRecord>();
            lst.Add(csvDataPoint1);
            lst.Add(csvDataPoint2);
            lst.Add(csvDataPoint3);
            lst.Add(csvDataPoint4);
            lst.Add(csvDataPoint5);
            touFile.ExcelData = lst;

            double expectedMedian = 9125.04;

            var expectedValuesAboveOrBelowMean = new List<double>();
            expectedValuesAboveOrBelowMean.Add(712.04);
            expectedValuesAboveOrBelowMean.Add(7172.04);
            expectedValuesAboveOrBelowMean.Add(345812.04);


            //Act

            double calculatedMean = touFile.FindMedian();

            var perAboveMedian = calculatedMean + calculatedMean * 0.2;
            var perBelowMedian = calculatedMean - calculatedMean * 0.2;
            var calculatedValuesAboveOrBelowMean = new List<double>();

            foreach (TOUCsvDataRecord model in touFile.ExcelData)
            {
                // values 20% above median
                if (model.Energy > 0 && (model.Energy > perAboveMedian))
                {
                    calculatedValuesAboveOrBelowMean.Add(model.Energy);
                }

                // values 20% above median
                if (model.Energy > 0 && (model.Energy < perBelowMedian))
                {
                    calculatedValuesAboveOrBelowMean.Add(model.Energy);
                }
            }

            //Assert

            Assert.AreEqual(expectedMedian, calculatedMean);
            CollectionAssert.AreEqual(expectedValuesAboveOrBelowMean, calculatedValuesAboveOrBelowMean);
        }
    }
}
