﻿using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.IO;
using CsvHelper;
using System;
using DataObjects;
using BusinessObjects;

namespace ERMPowerConsoleApp
{
    class Program
    {
        /// <summary>
        /// A console program to process LP or TOU Csv files as per the coding challenge.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            try
            {
                var csvFilePath = ConfigurationManager.AppSettings.Get("CSVFilePath");
                var processedData = DataFileProcessor.ProcessData(Directory.GetFiles(csvFilePath));

                foreach (CsvDataFile file in processedData)
                {
                    file.PrintMedian();
                }

                foreach (CsvDataFile file in processedData)
                {
                    file.Print20PercentAboveAndBelowStats();
                }

                Console.ReadLine();

            }
            catch (Exception ex)
            {
                Console.WriteLine("Problem occured: {0} ", ex.Message);
            }
        }
    }
}
