﻿using BusinessObjects;
using CsvHelper;
using DataObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERMPowerConsoleApp
{
    /// <summary>
    /// A helper class to import the required data from Csv files for processing. 
    /// </summary>
    public static class DataFileProcessor
    {
        /// <summary>
        /// Import data from LP and TOU Csv files. This method uses the CsvHelper NuGet package. 
        /// DataObjects project has the required class implementations required for CsvHelper.
        /// </summary>
        /// <param name="csvFiles">A list of Csv files paths</param>
        /// <returns></returns>
        public static List<CsvDataFile> ProcessData(string[] csvFiles)
        {
            List<CsvDataFile> CsvFiles = new List<CsvDataFile>();

            foreach (var csvFile in csvFiles)
            {
                using (var sr = new StreamReader(csvFile))
                {
                    CsvReader reader = new CsvReader(sr);
                    reader.Configuration.HasHeaderRecord = true;
                    reader.Configuration.RegisterClassMap<LPDataRecordMap>();
                    reader.Configuration.RegisterClassMap<TOUDataRecordMap>();

                    if (Path.GetFileName(csvFile).ToUpper().StartsWith("LP"))
                    {
                        var dataFile = new LPCsvFile();
                        dataFile.FileName = Path.GetFileName(csvFile);
                        dataFile.ExcelData = reader.GetRecords<LPCsvDataRecord>().ToList<LPCsvDataRecord>();
                        CsvFiles.Add(new CsvDataFile(dataFile));
                    }
                    else if (Path.GetFileName(csvFile).ToUpper().StartsWith("TOU"))
                    {
                        var dataFile = new TOUCsvFile();
                        dataFile.FileName = Path.GetFileName(csvFile);
                        dataFile.ExcelData = reader.GetRecords<TOUCsvDataRecord>().ToList<TOUCsvDataRecord>();
                        CsvFiles.Add(new CsvDataFile(dataFile));
                    }
                }
            }

            return CsvFiles;
        }
    }
}
