﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
    /// <summary>
    /// Interface that implements any type of Csv data file
    /// </summary>
    public interface ICsvDataFileType
    {
        string FileName { get; set; }
        double FindMedian();
        void PrintOutput();
        void PrintMedian();
    }
}
