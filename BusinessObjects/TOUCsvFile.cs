﻿using DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessObjects
{
    /// <summary>
    /// Represents the LP type Csv file.
    /// </summary>
    public class TOUCsvFile : ICsvDataFileType
    {
        //File name of the csv file
        public string FileName { get; set; }
        // A list of csv data from the excel file
        public List<TOUCsvDataRecord> ExcelData { get; set; }

        public TOUCsvFile()
        {

        }

        /// <summary>
        /// Calculate the median value of a number series.
        /// Uses the MathNet.Numerics NuGet package to calculate the median
        /// </summary>
        /// <returns></returns>
        public double FindMedian()
        {
            var values = ExcelData.Select(v => v.Energy);
            return MathNet.Numerics.Statistics.Statistics.Median(values);
        }

        /// <summary>
        /// Prints the median value to the console.
        /// </summary>
        public void PrintMedian()
        {
            Console.WriteLine("FileName {0} Median Value {1} \n", FileName, FindMedian());
        }

        /// <summary>
        /// Prints 20% above and below median values to the console.
        /// </summary>
        public void PrintOutput()
        {
            var median = FindMedian();
            var perAboveMedian = median + median * 0.2;
            var perBelowMedian = median - median * 0.2;

            foreach (TOUCsvDataRecord model in ExcelData)
            {
                // values 20% above median
                if (model.Energy > 0 && (model.Energy > perAboveMedian))
                {
                    //{file name} {datetime} {value} {median value}
                    Console.WriteLine("Value 20% above median {0} {1} {2} {3}", this.FileName, model.DateTime, model.Energy, median);
                }

                // values 20% above median
                if (model.Energy > 0 && (model.Energy < perBelowMedian))
                {
                    //{file name} {datetime} {value} {median value}
                    Console.WriteLine("Value 20% below median {0} {1} {2} {3}", this.FileName, model.DateTime, model.Energy, median);
                }
            }
        }
    }
}
