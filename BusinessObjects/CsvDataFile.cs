﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
    /// <summary>
    /// Represents the Csv file.
    /// </summary>
    public class CsvDataFile
    {
        ICsvDataFileType csvFile;

        public CsvDataFile(ICsvDataFileType _csvFile)
        {
            this.csvFile = _csvFile;
        }

        /// <summary>
        /// Prints the median for data values.
        /// </summary>
        public void PrintMedian()
        {
            csvFile.PrintMedian();
        }

        /// <summary>
        /// Prints 20% above and below median stats
        /// </summary>
        public void Print20PercentAboveAndBelowStats()
        {
            csvFile.PrintOutput();
        }

        /// <summary>
        /// Print statistical information for Csv data
        /// </summary>
        public void PrintStatistics()
        {
            csvFile.PrintMedian();
            csvFile.PrintOutput();
        }
    }
}
